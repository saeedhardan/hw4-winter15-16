package il.technion.cs236369.webserver.ISimpleServlet;

import java.io.PrintWriter;

public interface HttpSimpleServletResponse {
	
	PrintWriter getWriter();
	
	void setHeader(String name, String value);
}
