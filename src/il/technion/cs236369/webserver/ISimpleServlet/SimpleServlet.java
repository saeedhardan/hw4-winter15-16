package il.technion.cs236369.webserver.ISimpleServlet;

public interface SimpleServlet {

	void doGet(HttpSimpleServletRequest request,
			HttpSimpleServletResponse response);
}
